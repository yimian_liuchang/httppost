package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"net/http"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatal("httppost <file> <url>")
	}

	file,err := os.Open(os.Args[1])
	if err != nil { log.Fatal(err) }
	defer file.Close()

	resp,err := http.Post(os.Args[2], "", file)
	if resp != nil { defer resp.Body.Close() }
	if err != nil { log.Fatal(err) }

	fmt.Println(resp.Status)

	data,err := ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatal(err) }
	if len(data) > 0 {
		fmt.Println(string(data))
	}
}
